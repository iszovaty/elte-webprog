# ELTE Web prog.

Repository ELTE Webprog. kurzushoz

## Folder info

- JS folder JS Beadandóhoz
- php folder php beadandóhoz.

## JS

<hr />

### Lépések a projekt teszteléséhez.

Lépj be a js folder-be és indíts el egy HTTP szerver-t.
Nyisd meg az index.html-t egy böngészőben (pl: Chrome)


### Játék útmutató

1. Válassz ki egy pályát!
2. Nyomd meg a start gombot!
3. CSAPASD!!!

### Specifikáció

Minimálisan teljesítendő (enélkül nem fogadjuk el, 8 pont)

- [X] Egyéb: a "További elvárások" részben szereplő `README.md` fájl megfelelően kitöltve szerepel a feltöltött csomagban (0 pont)
- [X] Játéktér: egy pálya elemei helyesen megjelennek (1 pont)
- [X] Játéktér: a fehér cellákra kattintással villanykörte helyezhető el (1 pont)
- [X] Játéktér: a korábban elhelyezett villanykörte kattintással el is távolítható (1 pont)
- [X] Játéktér: a fekete cellákra nem lehet villanykörtét elhelyezni (1 pont)
- [X] Játéktér: a játék detektálja (automatikusan vagy gombra kattintva), ha az elkészített megoldás helyes (3 pont)
- [X] Játéktér: helyes megoldás után a játék újrakezdhető az oldal újratöltése nélkül (1 pont)

Az alap feladatok (12 pont)

- [X] Pályaválasztó: legalább három különböző előre elkészített pálya közül lehet választani, és a kiválasztott pálya helyesen elindul (1 pont)
- [ ] Pályaválasztó: megadható a játékos neve, amely a játékoldalon és mentéskor megjelenik (1 pont)
- [ ] Játéktér: játék közben folyamatosan látható az eltelt idő (1 pont)
- [X] Játéktér: minden megvilágított (vagy villanykörtét tartalmazó) fehér cella sárga háttérszínnel jelenik meg (1 pont)
- [ ] Játéktér: a fény terjedése animációval történik, vagyis látható, hogy milyen sorrendben kapnak sárga háttérszínt az egyes cellák a villanykörtétől távolodva (1 pont)
- [ ] Játéktér: külön stílussal (pl. zöld szövegszín) jelenik meg, ha egy fekete cellát a beleírt számnak megfelelő számú villanykörte határol (1 pont)
- [ ] Játéktér: külön stílussal (pl. piros szín vagy ikon) jelenik meg, ha két villanykörte egymást megvilágítja (1 pont)
- [ ] Játéktér: a játék menet közben megszakítható és menthető (1 pont)
- [ ] Pályaválasztó: láthatók a legutóbbi játékok eredményei: játékos neve, pálya neve, teljesítés ideje (1 pont)
- [ ] Pályaválasztó: a legutóbbi játékok eredményei megmaradnak az oldal bezárása után is (1 pont)
- [ ] Pályaválasztó: a pályaválasztó oldalon látszik, ha van mentett játék, és ez a játék helyesen betölthető (1 pont)
- [ ] Egyéb: igényes kialakítás (1 pont)

Plusz feladatok (plusz 5 pont)

- [ ] Szerkesztő: saját pályák létrehozhatók tetszőleges méretben és kiinduló mezőkkel (3 pont)
- [ ] Szerkesztő: a saját pályák localStorage-ben tartósan tárolódnak (1 pont)
- [ ] Szerkesztő: a mentett pálya később szerkesztésre újra megnyitható és módosítás után menthető (1 pont)


## PHP

<hr />

### Lépések a teszteléshez

1. Lépj be a php folder-ba.
2. Indítsd el egy HTTP szervert.
3. Nyisd meg az index.php-t és máris indulhat a konzultáció!!

Javaslat: (Docker telepítés & projekt tesztelhetősége végett)

```
docker run -dit --name phpsrv -p 8000:80 -v $PWD:/var/www/html php:7.4-apache
```

### Specifikáció

Minimálisan teljesítendő (enélkül nem fogadjuk el, 8 pont)

- [ ] Főoldal: megjelenik (0 pont)
- [ ] Főoldal: az összes létrehozott szavazólap megjelenik (1 pont)
- [ ] Főoldal: a szavazólapokon látszanak a szavazólapokhoz tartozó adatok: a szavazás sorszáma, a létrehozás ideje, a leadási határideje (1 pont)
- [ ] Főoldal: az oldalon külön látszódnak azok a szavazólapok, melyeknek a határideje már lejárt, és mindkét csoportot a létrehozási dátumuk szerint csökkenő sorrendben látjuk (tehát a legfrissebb van legfelül) (1 pont)
- [ ] Főoldal: egy szavazólapra rákattintva a szavazóoldalra jutunk (1 pont)
- [ ] Szavazóoldal: megjelennek a szavazólapokozhoz tartozó adatok, a szövegezése, és a választási lehetőségek, illetve a szavazás leadása gombbal le tudjuk adni a szavazatunkat (2 pont)
- [ ] Szavazóoldal: ha jól adtuk meg a szavazóoldalon lévő űrlapon az adatokat (tehát adtunk le szavazatot), akkor az oldal értesít minket a szavazás sikerességéről. Ha szavazat megjelölése nélkül próbáljuk meg elküldeni a szavazatunkat, jelzi a hibát. (0,5 pont)
- [ ] Szavazólap létrehozása: a szavazás létrehozó oldalnál meg lehet adni a szavazáshoz tartozó kérdést, választási lehetőségeket (kettőt, vagy többet), hogy mi legyen a szavazási leadási határideje, és elmenti azt is, hogy mikor lett létrehozva (1,5 pont)

Az alap feladatok (12 pont)


- [ ] Bejelentkezés: hibás esetek kezelése (1 pont)
- [ ] Bejelentkezés: sikeres bejelentkezés (1 pont)
- [ ] Regisztrációs űrlap: megfelelő elemeket tartalmazza (név, e-mail, jelszó, jelszó mégegyszer) (0,5 pont)
- [ ] Regisztrációs űrlap: hibás esetek kezelése, hibaüzenet, állapottartás (1,5 pont)
- [ ] Regisztrációs űrlap: sikeres regisztráció (0,5 pont)
- [ ] Főoldal: bejelentkezés nélkül a szavazólapok mellett lévő gomb a bejelentkező oldalra visz. Ha be vagyunk jelentkezve a gomb a szavazóoldalra visz. (1 pont)
- [ ] Főoldal: a gomb szövegén látszik, hogy szavaztunk-e már az adott szavazólapon (Ha szavaztunk már, akkor például a *Szavazat frissítése* jelenjen meg rajta) (0,5 pont)
- [ ] Főoldal: a szavazólapon a leadási határidő után nem tudjuk módosítani a szavazatunkat, és a határidő után látszik csak a szavazás eredménye (1 pont)
- [ ] Admin funkció: be lehet jelentkezni az admin felhasználó adataival (0,5 pont)
- [ ] Admin funkció: új szavazólap létrehozása CSAK az admin felhasználóval lehetséges (0,5 pont)
- [ ] Admin funkció: a szavazólap létrehozásánál be lehet állítani, hogy az opciókból egyet vagy többet tudjon a kitöltő kiválasztani (radio vagy checkbox) (1 pont)
- [ ] Admin funkció: meglévő szavazólap törlése admin felhasználóval (1 pont)
- [ ] Szavazóoldal: a szavazóoldalon látszik, hogy egy szavazólapon egy vagy több dologra tudunk szavazni; és ha lehet többre, több dologra is le tudjuk adni a szavazatunkat, amely helyesen el is tárolódik (2 pont)
- [ ] **Késés: -0,5 pont / megkezdett nap!**

Plusz feladatok (plusz 5 pont)

- [ ] Admin funkció: a létező felhasználók között az admin csoportokat tud létrehozni, a szavazólap létrehozásánál ezekhez a csoportokhoz egy kattintással hozzá tudja rendelni (3 pont)
- [ ] Admin funkció: Az admin bejelentkezése esetén megjelenik egy plusz gomb a Főoldalon a szavazólapoknál, amiknek a segítségével módosítani tudja a feladatlapokhoz tartozó adatokat, és ezek frissülnek a tárolt adataink között. (2 pont)