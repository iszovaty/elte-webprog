import { drawingElement, heightInput, widthInput } from "./references.js";
import { state } from "./state.js";
import { renderDrawing } from "./template.js";

export function onStartClick() {
  const width = widthInput.valueAsNumber;
  const height = heightInput.valueAsNumber;

  state.initDrawing(width, height);

  drawingElement.innerHTML = renderDrawing(state.drawing);
}


export function onDrawingCellClick(event) {

  if (!event.target.matches("td")) {
    return;
  }

  const cell = event.target;
  //const x = cell.dataset.x;
  //const y = cell.dataset.y; //data attributes getter.
  const x = cell.cellIndex;
  const y = cell.parentNode.rowIndex;
  const color = colorInput.value;

  console.log({x, y});

  state.changeCellColor(x, y, color);

  drawingElement.innerHTML = renderDrawing(state.drawing);
}


export function onColorPickerChange() {
  const color = colorPicker.value;

  state.addColorToColorHistory(color);

  colorHistoryElement.innnerHTML = renderColorHistory(state.colorHistory);
}