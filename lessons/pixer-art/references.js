export const startButton = document.querySelector("#start");
export const widthInput = document.querySelector("#width");
export const heightInput = document.querySelector("#height");
export const drawingElement = document.querySelector("#drawing");
export const colorInput = document.querySelector("#color-picker");