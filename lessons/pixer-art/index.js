import { onColorPickerChange, onDrawingCellClick, onStartClick } from "./events.js";
import { drawingElement, startButton } from "./references.js";

startButton.addEventListener("click", onStartClick);

drawingElement.addEventListener("click", onDrawingCellClickngCellClick);

colorPicker.addEventListener("change", onColorPickerChangeickerChange);