export function renderDrawing(drawing) {
  return `
    <table>${drawing.map(renderRow).join("\n")}</table>
  `;
}

function renderRow(row, rowIndex) {
  return `
    <tr>${row.map(renderCell).join("\n")}</tr>
  `;
}

function renderCell(cell) {
  return `
    <td data-x="" data-y=""></td>
  `;
}


