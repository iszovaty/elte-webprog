const form = document.querySelector("form");
const divContainer = document.querySelector(".container");
const allInputs = document.querySelectorAll('input');

function getLastYearConsumptionSum() {
    
    var consumptionSum = 0;
    for (var i = 0; i < allInputs.length; i++) {
        consumptionSum += parseInt(allInputs[i].dataset.consumption);
    }

    return consumptionSum;
}

//ez itt a 2.a)
console.log(getLastYearConsumptionSum());

function getAllInputsConsumption() {

    var consumptions = [];
    for (var i = 0; i < allInputs.length; i++) {

        var value = parseInt(allInputs[i].value);
        var max = parseInt(allInputs[i].max);
        var consumption = parseInt(allInputs[i].dataset.consumption);

        var ci = parseFloat((value / max) * consumption).toFixed(2);
        consumptions[i] = ci;
    }

    return consumptions;
}


function getConsumptionPercentageById(id) {
    var selector = '#' + id;
    var consumptionInput = document.querySelector(selector);

    var value = parseInt(consumptionInput.value);
    var max = parseInt(consumptionInput.max);
    var consumption = parseInt(consumptionInput.dataset.consumption);
    
    var ci = parseFloat((value / max) * consumption).toFixed(2)
    var m = getLastYearConsumptionSum();
    var width = parseInt(ci / m * 100);

    return width;
}

function onCalculateConsumptions(event) {

    var idValue = event.target.id;
    var allConsumptions = getAllInputsConsumption();
    console.log(allConsumptions);

    var labels = document.getElementsByTagName('label');
    for (var i = 0; i < labels.length; i++) {
        
        if (idValue === labels[i].htmlFor) {
            
            var actWidth = getConsumptionPercentageById(idValue);
            labels[i].style.width = actWidth + '%';
        }
    }

}

for (var i = 0; i < allInputs.length; i++) {
    allInputs[i].addEventListener('input', onCalculateConsumptions)
}

