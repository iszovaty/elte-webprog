const task1 = document.querySelector('#task1');
const task2 = document.querySelector('#task2');
const task3 = document.querySelector('#task3');
const task4 = document.querySelector('#task4');

const game = [
  "XXOO",
  "O OX",
  "OOO ",
];


function doAllRowsHaveSameLength() {

  var sameLength = true;
  for (var i = 1; i < game.length && sameLength; i++) {
    if (game[i - 1].length !== game[i].length) {
      sameLength = false;
    }
  }

  return sameLength;
}

function isFirstRowOnlyContainsXorO() {

  var onlyContainsXorO = true;
  var firstRow = game[0];
  for (var i = 0; i < firstRow.length && onlyContainsXorO; i++) {

    if (firstRow[i] !== 'X' && firstRow[i] !== 'O') {
      onlyContainsXorO = false;
    }
  }

  return onlyContainsXorO;
}

function numbersOfXandO() {
  
  var xCounter = 0;
  var oCounter = 0;
  for (var i = 0; i < game.length; i++) {
    
    var row = game[i];

    for (var j = 0; j < row.length; j++) {
      
      if (row[j] === 'X') {
        xCounter++;
      }

      if (row[j] === 'O') {
        oCounter++;
      }
    }
  }

  return `X / O = ${xCounter} / ${oCounter}`;
}


function indexWhere3FollowedXorO() {

  var found = false;
  var index = -1;
  for (var i = 0; i < game.length && !found; i++) {

    var row = game[i];

    if (row.includes("XXX") || row.includes("OOO")) {
      index = i;
      found = true;
    }
  }

  return index;
}


task1.textContent = doAllRowsHaveSameLength();
task2.textContent = isFirstRowOnlyContainsXorO();
task3.textContent = numbersOfXandO();

var followed3XorOIndex = indexWhere3FollowedXorO();
if ( followed3XorOIndex !== -1) {
  task4.textContent = followed3XorOIndex;
}

