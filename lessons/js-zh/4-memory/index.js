const inputCircleNumber = document.querySelector("#circle-number");
const buttonStart = document.querySelector("#start");
const divContainer = document.querySelector("#container");
const divOutput = document.querySelector("#output");

// Application state

let canGuess = false;
let solution = [];
let series = [];

// ========= Utility functions =========

function random(a, b) {
  return Math.floor(Math.random() * (b - a + 1)) + a;
}

function toggleHighlight(node) {
  node.classList.toggle("highlight")
  node.addEventListener("animationend", function (e) {
    node.classList.remove("highlight");
  }, {once: true});
}

// =====================================

function renderDivContainerHtml(value) {

  //Ez itt a kert...
  //`<a class="circle"></a>`;
  var numberOfCircles = parseInt(value); 
  var html = "";
  for (var i = 0; i < numberOfCircles; i++) {
    var number = i + 1;
    html += `<a class="circle">${number}</a>`
  }

  return html;
}

function onCircleInputChange(event) {

  var item = event.target;

  var value = event.target.value;

  divContainer.innerHTML = renderDivContainerHtml(value);
}

function onButtonStartClick() {
  for (var i = 0; i < 7; i++) {

    var numberOfCircles = document.getElementsByClassName('circle').length;
    console.log(numberOfCircles);

    series[i] = parseInt((Math.random() * numberOfCircles) + 1);
  }

  console.log(series);

  // Ez itt a 4.c) -> emiatt 1-el többvillanás lesz majd elvileg mint amennyi a játékhoz kéne (gondolom....)
  animateFirstCirlce();

  // Emmeg itt a 4.d)
  animateCirlces();

  var circles = document.getElementsByClassName('circle');
  for (var i = 0; i < circles.length; i++) {
    circles[i].addEventListener('click', onCircleClick)
  }
}

function animateFirstCirlce() {

  var firstCirlce = document.querySelector('a:nth-child(1)');
  toggleHighlight(firstCirlce);
}

function getNthCirlce(n) {

  var circleSelector = 'a:nth-child(' + n + ')'
  return document.querySelector(circleSelector);
}

function animateCirlces(i = 0) {

  if (i < series.length) {

    toggleHighlight(getNthCirlce(series[i]));
    setTimeout(() => animateCirlces(i + 1), 1000);
  }
}


inputCircleNumber.addEventListener('input', onCircleInputChange);
buttonStart.addEventListener('click', onButtonStartClick);


function onCircleClick(event) {
  var numbOfPressedCircle = parseInt(event.target.textContent);
  solution.push(numbOfPressedCircle);
  console.log(solution);



  if (solution.length === series.length) {
    console.log("THE SERIES AND SOLUTION ARRAY LENGTHS ARE MATHING");

    var goodSolution = true;
    for (var i = 0; i < solution.length && goodSolution; i++) {
      if (solution[i] !== series[i]) {
        goodSolution = false;
      }
    }
    

    if (goodSolution) {
      console.log("GOOD SOLUTION");
    } else {
      console.log("BAD SOLUTION! TRY AGAIN NOOB!");
    }
  }

}

