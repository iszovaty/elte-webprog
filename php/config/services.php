<?php

declare(strict_types=1);

use FW\Container\Container;
use FW\Service\View\ViewEngine;

return [
    'parameters' => [
        'project_dir' => dirname(__DIR__),
        'source_dir' => dirname(__DIR__) . '/src/',
        'view_dir' => dirname(__DIR__) . '/views/'
    ],
    'services' => [
        ViewEngine::class => function (Container $container) {
            return new ViewEngine($container->getParameter('view_dir'));
        }
    ]
];
