<?php

declare(strict_types=1);

namespace FW\Entity;

class BaseEntity
{
    private ?int $id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): BaseEntity
    {
        $this->id = $id;

        return $this;
    }
}