<?php

declare(strict_types=1);

namespace FW\Entity;

use DateTime;

class VoteSheet extends BaseEntity
{

    private ?string $title;

    private ?DateTime $createdAt;

    private ?DateTime $expirationDate;

    private array $options = [];

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): VoteSheet
    {
        $this->title = $title;

        return $this;
    }

    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?DateTime $createdAt): VoteSheet
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getExpirationDate(): DateTime
    {
        return $this->expirationDate;
    }

    public function setExpirationDate(?DateTime $expirationDate): VoteSheet
    {
        $this->expirationDate = $expirationDate;

        return $this;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function setOptions(?array $options): VoteSheet
    {
        $this->options = $options;

        return $this;
    }
}