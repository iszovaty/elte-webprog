<?php

declare(strict_types=1);

namespace FW\Controller;

use FW\Http\Response;

class VoteSheetController extends AbstractController
{
    public function listAction(): Response
    {
        // business logic...

        return $this->render('list', [
            'title' => 'Szavazólap Lista',
            'testArray' => [
                'A', 'B', 'C', 'D', 'E', 'F'
            ]
        ]);
    }
}