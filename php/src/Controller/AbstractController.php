<?php

declare(strict_types=1);

namespace FW\Controller;

use FW\Http\Response;
use FW\Service\View\ViewEngine;

abstract class AbstractController
{
    private ViewEngine $viewEngine;

    public function __construct(ViewEngine $viewEngine)
    {
        $this->viewEngine = $viewEngine;
    }

    protected function render(string $template, array $parameters = []): Response
    {
        return $this->viewEngine->render($template, $parameters);
    }
}