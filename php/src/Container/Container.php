<?php

declare(strict_types=1);

namespace FW\Container;

use Closure;
use ReflectionClass;
use ReflectionException;

class Container
{
    private array $parameters = [];
    private array $services = [];
    private array $aliases = [];

    # region parameters

    public function addParameter(string $key, $value): void
    {
        if (isset($this->parameters[$key])) {
            return;
        }

        $this->parameters[$key] = $value;
    }

    public function hasParameter(string $key): bool
    {
        return isset($this->parameters[$key]);
    }

    public function getParameter(string $key)
    {
        if (!$this->hasParameter($key)) {
            return null;
        }

        return $this->parameters[$key];
    }

    public function getParameters(): array
    {
        return [
            'parameters' => array_keys($this->parameters)
        ];
    }

    # endregion parameters

    # region services

    public function addService(string $name, Closure $closure, ?string $alias = null): void
    {
        if (isset($this->services[$name])) {
            return;
        }

        $this->services[$name] = $closure;

        if ($alias) {
            $this->addAlias($alias, $name);
        }
    }

    public function hasService(string $name): bool
    {
        return isset($this->services[$name]);
    }

    public function getService(string $name)
    {
        if (!$this->hasService($name)) {
            return null;
        }

        // Closure feloldás
        if ($this->services[$name] instanceof Closure) {
            $this->services[$name] = $this->services[$name]($this);
        }

        return $this->services[$name];
    }

    public function getServices(): array
    {
        return [
            'services' => array_keys($this->services),
            'aliases' => $this->aliases
        ];
    }

    # endregion services

    # region aliases

    protected function addAlias(string $alias, string $name): void
    {
        $this->aliases[$alias] = $name;
    }

    public function hasAlias(string $alias): bool
    {
        return isset($this->aliases[$alias]);
    }

    public function getAlias(string $name)
    {
        return $this->getService($this->aliases[$name]);
    }

    # region aliases

    public function loadServices(string $baseDir, string $namespace): void
    {
        if (null === ($files = $this->getServiceFiles($baseDir, $namespace))) {
            return;
        }

        foreach ($files as $file) {
            try {
                $class = new ReflectionClass($namespace . '\\' . basename($file, '.php'));
            } catch (ReflectionException $exception) {
                continue;
            }

            $serviceName = $class->getName();

            // Ha abstract osztályról van szó vagy ha már létezik az adott service, akkor nem kell foglalkoznunk vele
            if ($class->isAbstract() || $class->isInterface() || $this->hasService($serviceName)) {
                continue;
            }

            $arguments = [];
            if (null !== ($constructor = $class->getConstructor())) {
                // Az adott osztály / service konstruktor paraméterei
                $arguments = $constructor->getParameters();
            }

            $serviceParameters = [];
            foreach ($arguments as $argument) {
                $type = (string)$argument->getType();

                // Ha a konstruktor paraméternek megfelelő típus már a service containerben van
                // Akkor adjuk hozzá
                if ($this->hasService($type) || $this->hasAlias($type)) {
                    $serviceParameters[] = $this->getService($type) ?? $this->getAlias($type);
                } else {
                    // Ha nincs, akkor egy closure - t adjunk hozzá, ez lehetővé teszi, hogy később a service containerhez adott service - t beállítsunk
                    $serviceParameters[] = function (Container $container) use ($type) {
                        return $this->getService($type) ?? $this->getAlias($type);
                    };
                }
            }

            $this->addService($serviceName, function () use ($serviceName, $serviceParameters) {
                foreach ($serviceParameters as $serviceParameter) {
                    if ($serviceParameter instanceof Closure) {
                        $serviceParameter = $serviceParameter($this);
                    }
                }

                return new $serviceName(...$serviceParameters);
            });
        }
    }

    private function getServiceFiles(string $baseDir, string $namespace): ?array
    {
        $directory = str_replace('\\', '/', $namespace);
        // Névtér prefix levágása ( absolute útvonal összeállítása )
        $directory = $baseDir . substr($directory, strpos($directory, '/') + 1);

        // A mappában lévő fájlok listája
        // Ha nincs fájl, térjünk vissza
        if (false === ($files = scandir($directory))) {
            return null;
        }

        // Invalid értékek kiszűrése
        return array_filter($files, function ($file) {
            return $file !== '.' && $file !== '..';
        });
    }
}