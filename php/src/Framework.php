<?php

declare(strict_types=1);

namespace FW;

use Exception;
use FW\Http\Request;
use FW\Http\Response;
use FW\Container\Container;

class Framework
{
    private ?Container $container = null;
    private array $routes;
    private array $services;

    public function __construct(array $routes, array $services)
    {
        $this->routes = $routes;
        $this->services = $services;
    }

    private function preHandle(): void
    {
        $container = new Container();

        $this->buildParameters($container);
        $this->buildServices($container);

        $this->container = $container;
    }

    private function buildParameters(Container $container): void
    {
        foreach ($this->services['parameters'] as $key => $value) {
            $container->addParameter($key, $value);
        }
    }

    private function buildServices(Container $container): void
    {
        foreach ($this->services['services'] as $alias => $service) {
            $container->addService($alias, $service);
        }

        $sourceDirectory = $container->getParameter('source_dir');

        $container->loadServices($sourceDirectory, 'FW\\Controller');
        $container->loadServices($sourceDirectory, 'FW\\Service');
    }

    public function handle(Request $request): Response
    {
        try {
            $this->preHandle();

            if ($route = $this->routes[$request->getPath()] ?? null) {
                if (null !== ($controller = $this->container->getService($route['_controller']))) {
                    /** @var Response $response */
                    $response = call_user_func([$controller, $route['_method']], $request);
                    return $response;
                }
            }
        } catch (Exception $exception) {
            return new Response('An error occurred', 500, 'Internal Server Error');
        }

        return new Response('Not Found', 404, 'Not Found');
    }
}