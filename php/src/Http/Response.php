<?php

declare(strict_types=1);

namespace FW\Http;

class Response
{
    private string $content;
    private int $code;
    private string $codeName;

    public function __construct(string $content = '', int $code = 200, string $codeName = 'OK')
    {
        $this->content = $content;
        $this->code = $code;
        $this->codeName = $codeName;
    }

    public function send(): Response
    {
        // TODO: headers
        header(sprintf('HTTP/1.1 %s %s', $this->code, $this->codeName));
        header('Content-Type: text/html; charset=utf-8');

        echo $this->content;

        return $this;
    }
}