<?php

declare(strict_types=1);

namespace FW\Http;

class Request
{
    const DEFAULT_PATH = '/';

    private ?string $path = null;

    private array $query = [];

    public static function createFromGlobals(): Request
    {
        $uri = htmlspecialchars($_SERVER['REQUEST_URI'] ?? '', ENT_QUOTES, 'UTF-8');

        return (new self())
            ->setPath(parse_url($uri, PHP_URL_PATH))
            ->buildQueryArrayFromString(parse_url($uri, PHP_URL_QUERY));
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(?string $path): Request
    {
        $this->path = $path;
        return $this;
    }

    public function getQuery(): array
    {
        return $this->query;
    }

    public function setQuery(array $query): Request
    {
        $this->query = $query;
        return $this;
    }

    public function buildQueryArrayFromString(?string $queryString): Request
    {
        if (null === $queryString) {
            return $this;
        }

        $parameters = [];
        $queryString = explode('&amp;', $queryString);

        foreach ($queryString as $query) {
            $parameterParts = array_values(explode('=', $query));
            $parameters[$parameterParts[0]] = $parameterParts[1];
        }

        $this->setQuery($parameters);

        return $this;
    }
}