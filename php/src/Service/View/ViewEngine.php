<?php

declare(strict_types=1);

namespace FW\Service\View;

use FW\Http\Response;

class ViewEngine
{
    private string $viewDirectory;

    public function __construct(string $viewDirectory)
    {
        $this->viewDirectory = $viewDirectory;
    }

    public function render(string $template, array $parameters = []): Response
    {
        $filename = $this->viewDirectory . $template . '.php';

        if (!file_exists($filename)) {
            return new Response('Not Found', 404, 'Not Found');
        }

        // Fájl tartalmának betöltése - vagy ez a megoldás vagy lehet másfele kacsingatni...
        extract($parameters, EXTR_SKIP);
        ob_start();
        include $filename;

        return new Response(ob_get_clean());
    }
}