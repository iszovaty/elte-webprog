<?php

declare(strict_types=1);

namespace FW\Service;

interface DataHandlerInterface
{
    public function loadData(): void;

    public function saveDate(array $data): void;
}