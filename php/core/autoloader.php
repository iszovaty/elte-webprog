<?php

declare(strict_types=1);

const NAMESPACE_PREFIX = 'FW\\';
const SOURCE_DIRECTORY = 'src';

spl_autoload_register(function (string $class) {
    $baseDirectory = dirname(__DIR__) . '/' . SOURCE_DIRECTORY . '/';

    $len = strlen(NAMESPACE_PREFIX);
    if (strncmp(NAMESPACE_PREFIX, $class, $len) !== 0) {
        return;
    }

    // relative class név, prefix levágása
    $relativeClass = substr($class, $len);

    // '\' lecserélése könyvtár szeparátorra '/'
    // FW\Controller\HomeController ---> /src/Controller/HomeController.php
    $file = $baseDirectory . str_replace('\\', '/', $relativeClass) . '.php';

    if (file_exists($file)) {
        require $file;
    }
});
