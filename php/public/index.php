<?php

declare(strict_types=1);

require_once dirname(__DIR__) . '/core/autoloader.php';

use FW\Framework;
use FW\Http\Request;

$request = Request::createFromGlobals();

$routes = include dirname(__DIR__) . '/config/routes.php';
$services = include dirname(__DIR__) . '/config/services.php';

$framework = new Framework($routes, $services);
$response = $framework->handle($request);
$response->send();

die();
