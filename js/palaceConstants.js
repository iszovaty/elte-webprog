
// SELECTION OPTIONS
export const EASY_7X7 = '7x7_easy';
export const HARD_7X7 = '7x7_hard';
export const EXTREME_10X10 = '10x10_extreme';


export const RESULT_TYPE_LOSE = 'TYPE_LOSE';
export const RESULT_TYPE_WIN = 'TYPE_WIN';

export const RESULT_BOARD_TEXT_WIN = 'GG! Well Played! You won!';
export const RESULT_BOARD_TEXT_LOSE = 'Game Over! You lose!';

// MAPS (FOR LATER REFACTOR...)

export const map_7x7_easy = [
    ['C', 'C', 'C', '1', 'C', 'C', 'C'],
    ['C', '0', 'C', 'C', 'C', '2', 'C'],
    ['C', 'C', 'C', 'C', 'C', 'C', 'C'],
    ['W', 'C', 'C', 'W', 'C', 'C', 'W'],
    ['C', 'C', 'C', 'C', 'C', 'C', 'C'],
    ['C', 'w', 'C', 'C', 'C', '2', 'C'],
    ['C', 'C', 'C', '3', 'C', 'C', 'C']
];

export const map_7x7_hard = [
    ['C', 'C', '0', 'C', 'W', 'C', 'C'],
    ['C', 'C', 'C', 'C', 'C', 'C', 'C'],
    ['W', 'C', 'W', 'C', '3', 'C', 'W'],
    ['C', 'C', 'C', '1', 'C', 'C', 'C'],
    ['2', 'C', 'W', 'C', 'W', 'C', 'W'],
    ['C', 'C', 'C', 'C', 'C', 'C', 'C'],
    ['C', 'C', 'W', 'C', '2', 'C', 'C']
];

export const map_10x10_extreme = [
    ['C', 'W', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C'],
    ['C', 'C', 'C', 'C', 'C', '3', 'C', '2', 'C', 'W'],
    ['C', '0', 'W', 'C', 'C', 'C', 'C', 'W', 'C', 'C'],
    ['C', 'C', 'C', 'C', 'W', 'C', 'C', 'C', 'C', 'C'],
    ['C', '1', 'C', 'C', 'W', '1', 'W', 'C', 'C', 'C'],
    ['C', 'C', 'C', 'W', 'W', 'W', 'C', 'C', '3', 'C'],
    ['C', 'C', 'C', 'C', 'C', 'W', 'C', 'C', 'C', 'C'],
    ['C', 'C', '1', 'C', 'C', 'C', 'C', '0', 'W', 'C'],
    ['3', 'C', 'W', 'C', '0', 'C', 'C', 'C', 'C', 'C'],
    ['C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', '0', 'C']
];