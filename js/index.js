import { onStartClick, onResartClick } from './handlers.js';
import  { startBtn, restartBtn }  from './references.js';

// define handlers..
startBtn.addEventListener('click', onStartClick);

restartBtn.addEventListener('click', onResartClick);


