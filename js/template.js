import { EASY_7X7 } from "./palaceConstants.js";
import { HARD_7X7 } from "./palaceConstants.js";
import { EXTREME_10X10 } from "./palaceConstants.js";


// ##############################################################################################
// HARD CODED TABLE GENS....


export function generateTable(mapType) {

    if (mapType === EASY_7X7) {
        return createTable(7, generate7x7EasyCellOrWall);
    } 
    
    if (mapType === HARD_7X7) {
        return createTable(7, generate7x7HardCellOrWall);
    } 

    if (mapType === EXTREME_10X10)  {
        return createTable(10, generate10x10ExtremeCellOrWall);
    }

    return createTable(7, generate7x7EasyCellOrWall);
}


function createTable(n, generateCellOrWallFunction) {

    let table = `<table>`
    for (let i = 0; i < n; i++) {
        
        table += `<tr>`;
        for (let j = 0; j < n; j++) {
            table += generateCellOrWallFunction(i, j);
        }
        table += `</tr>`;
    }

    return table;
}


export function placeBulbOnCell() {
    return `<img class="bulb" src="img/bulb.png" />`;
}

export function removeBulbFromCell() {
    return ``;
}



//      ### 7x7 CELL VIEWS
//
//##########################################################################################################
//



//      7X7 EASY VIEW

function generate7x7EasyCellOrWall(row, col) {
    
    if (row === 0 && col === 3) {

        return `<td class='cell wall wall-1' data-row=${row} data-col=${col}>1</td>`;
    }

    if (row === 1 && col === 1) {

        return `<td class='cell wall wall-0' data-row=${row} data-col=${col}>0</td>`;
    }

    if ( (row === 1 && col === 5) ||
         (row === 5 && col === 5)) {
            
        return `<td class='cell wall wall-2' data-row=${row} data-col=${col}>2</td>`;
    }

    if ( (row === 3 && col === 0) ||
         (row === 3 && col === 3) ||
         (row === 3 && col === 6) ||
         (row === 5 && col === 1) ) {

        return `<td class='cell wall' data-row=${row} data-col=${col}></td>`;
    } 

    if (row === 6 && col === 3) {

        return `<td class='cell wall wall-3' data-row=${row} data-col=${col}>3</td>`;
    }

    return `<td class='cell room' data-row=${row} data-col=${col} ></td>`;
}

// 7x7 HARD VIEW

function generate7x7HardCellOrWall(row, col) {

    if ( (row === 0 && col === 4) ||
         (row === 2 && col === 0) || 
         (row === 2 && col === 2) ||
         (row === 2 && col === 6) ||
         (row === 4 && col === 2) ||
         (row === 4 && col === 4) || 
         (row === 4 && col === 6) ||
         (row === 6 && col === 2) ) {

        return `<td class='cell wall' data-row=${row} data-col=${col}></td>`;
    }

    if ( (row === 4 && col === 0) ||
         (row === 6 && col === 4) ) {
        
        return `<td class='cell wall wall-2' data-row=${row} data-col=${col}>2</td>`;
    }

    if (row === 0 && col === 2) {
        return `<td class='cell wall wall-0' data-row=${row} data-col=${col}>0</td>`;
    }

    if (row === 2 && col === 4) {
        return `<td class='cell wall wall-3' data-row=${row} data-col=${col}>3</td>`;
    }

    if (row === 3 && col === 3) {
        return `<td class='cell wall wall-1' data-row=${row} data-col=${col}>1</td>`;
    }

    return `<td class='cell room' data-row=${row} data-col=${col}></td>`;
}



// 10x10 EXTREME VIEW (THE NIGHTMARE MODE)
function generate10x10ExtremeCellOrWall(row, col) {

    if ( (row === 0 && col === 1) ||
         (row === 1 && col === 9) ||
         (row === 2 && col === 2) ||
         (row === 2 && col === 7) ||
         (row === 3 && col === 4) ||
         (row === 4 && col === 4) ||
         (row === 4 && col === 6) ||
         (row === 5 && col === 3) ||
         (row === 5 && col === 4) ||
         (row === 5 && col === 5) ||
         (row === 6 && col === 5) ||
         (row === 7 && col === 8) ||
         (row === 8 && col === 2) ) {

        return `<td class='cell wall' data-row=${row} data-col=${col}></td>`;
    } 

    if ( (row === 2 && col === 1) || 
         (row === 7 && col === 7) ||
         (row === 8 && col === 4) ||
         (row === 9 && col === 8) ) {

        return `<td class='cell wall wall-0' data-row=${row} data-col=${col}>0</td>`;
    }

    if ( (row === 4 && col === 1) || 
         (row === 4 && col === 5) || 
         (row === 7 && col === 2) ) {

        return `<td class='cell wall wall-1' data-row=${row} data-col=${col}>1</td>`;
    }
    
    if ( row === 1 && col === 7 ) {
        return `<td class='cell wall wall-2' data-row=${row} data-col=${col}>2</td>`;
    }

    if ( (row === 1 && col === 5) || 
         (row === 5 && col === 8) || 
         (row === 8 && col === 0) ) {
        
        return `<td class='cell wall wall-3' data-row=${row} data-col=${col}>3</td>`;
    }

    return `<td class='cell room' data-row=${row} data-col=${col}></td>`;
}
