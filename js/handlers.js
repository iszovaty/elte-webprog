import { placeBulbOnCell } from "./template.js";
import { service } from "./service.js";
import { RESULT_TYPE_LOSE, RESULT_TYPE_WIN } from "./palaceConstants.js";

// EVENT HANDLERS

export function onStartClick() {

    service.initGame();
}

export function onRoomCellClick(event) {

    var selectedRow = event.target.dataset.row;
    var selectedCol = event.target.dataset.col;
    
    if (event.target.attributes[0].value === 'cell room got-bulb') {

        console.log("removing bulb");
        event.target.setAttribute('class', 'cell room');
        
    } else {
        console.log("NO BULB YET");
        if (service.isPossibleToPlaceBulb(selectedRow, selectedCol)) {

            event.target.setAttribute('class', 'cell room got-bulb');

            service.lightUpNeighborRooms(selectedRow, selectedCol);
    
            if (service.hasNoRemainingDarkRooms()) {
    
                var numberOfRows = document.getElementsByTagName('tr').length;
    
                console.log("NO MORE DARK ROOMS!!!!!");
    
                if (service.checkAllWallsContraints(numberOfRows)) {
    
                    service.removeCellClickListeners();
                    service.updateResultBoard(RESULT_TYPE_WIN);
                
                } else {
    
                    service.removeCellClickListeners();
                    service.updateResultBoard(RESULT_TYPE_LOSE);
    
                }
            }
    
        } else {
    
            event.target.setAttribute('class', 'cell room got-bulb');
    
            service.lightUpNeighborRooms(selectedRow, selectedCol);
            service.removeCellClickListeners();
            service.updateResultBoard(RESULT_TYPE_LOSE)
        }
    }
}

export function onResartClick() {

    service.initGame();
    service.resetResultBoard();
}