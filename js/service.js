import { generateTable } from "./template.js";
import { palace } from "./references.js";
import { selection } from "./references.js";
import { RESULT_TYPE_WIN, RESULT_TYPE_LOSE, RESULT_BOARD_TEXT_WIN, RESULT_BOARD_TEXT_LOSE } from "./palaceConstants.js";
import { onRoomCellClick } from "./handlers.js";

export const service = {

    initGame() {

        const mapType = selection.options[selection.options.selectedIndex].value;
        palace.innerHTML = this.constructPalaceByType(mapType);
        this.constructCellEvents();
    },
    
    constructPalaceByType(mapType) {
    
        return generateTable(mapType);
    },
    
    constructCellEvents() {
    
        const cells = document.getElementsByClassName("room");
        for (var i = 0; i < cells.length; i++) {
            cells[i].addEventListener('click', onRoomCellClick);
        }
    },
    
    getTable() {
    
        let table = [];
        let rows = document.getElementsByTagName('tr');
    
        let cells = document.getElementsByTagName('td');
        var n = 0;
    
        for (var i = 0; i < rows.length; i++) {
            
            table[i] = [];
            for(var j = 0; j < rows.length; j++) {
                table[i][j] = cells[n];
                n++;
            }
        }
    
        return table;
    },
    
    getWallsByNumber(number) {
        let walls = document.getElementsByClassName('wall-0');
    },
    
    // BULD PLACEMENT VALIDATION

    isPossibleToPlaceBulb(selectedRow, selectedCol) {
    
        let table = this.getTable();   
    
        if ( !this.checkBulbPlacementAvailabilityVerticalBefore(table, selectedRow, selectedCol) ||
             !this.checkBulbPlacementAvailabilityVerticalAfter(table, selectedRow, selectedCol) ||
             !this.checkBulbPlacementAvailabilityHorizontalBefore(table, selectedRow, selectedCol) ||
             !this.checkBulbPlacementAvailabilityHorizontalAfter(table, selectedRow, selectedCol) ) {
    
            return false;
        }
    
        return true;
    },
    
    checkBulbPlacementAvailabilityVerticalBefore(table, selectedRow, selectedCol) {
    
        // column check before selected row
        var ok = true;
        var start = parseInt(selectedRow);
        for (var i = start; i >= 0 && ok; i--) {
    
            let cellClass = table[i][selectedCol].attributes[0].value;
            if ( cellClass.includes("got-bulb") ) {
                ok = false;
            }
    
            if ( cellClass.includes("wall") ) {
                return true;
            }
        }
    
        return ok;
    },
    
    checkBulbPlacementAvailabilityVerticalAfter(table, selectedRow, selectedCol) {
    
        //column check after selected row
        var ok = true;
        var start = parseInt(selectedRow);
        table = this.getTable();
        for (var i = start; i < table[selectedRow].length && ok; i++) {
    
            let cellClass = table[i][selectedCol].attributes[0].value;
            if ( cellClass.includes("got-bulb") ) {
                
                ok = false;
            }
    
            if ( cellClass.includes("wall") ) {
                return true;
            }
        }
    
        return ok;
    },
    
    checkBulbPlacementAvailabilityHorizontalBefore(table, selectedRow, selectedCol) {
    
        //row check before selected col
        var ok = true;
        var start = parseInt(selectedCol);
        
        for (var j = start; j >= 0 && ok; j--) {
    
            let cellClass = table[selectedRow][j].attributes[0].value;
            if ( cellClass.includes("got-bulb") ) {
                ok = false;
            }
    
            if ( cellClass.includes("wall") ) {
                return true;
            }
        }
    
        return ok;
    },
    
    checkBulbPlacementAvailabilityHorizontalAfter(table, selectedRow, selectedCol) {
    
        //row check after selected row
        var ok = true;
        var start = parseInt(selectedCol);
        for (var j = start; j < table[selectedRow].length && ok; j++) {
            
            let cellClass = table[selectedRow][j].attributes[0].value;
            if ( cellClass.includes("got-bulb") ) {
                ok = false;
            }
    
            if ( cellClass.includes("wall") ) {
                return true;
            }
        }
    
        return ok;
    },
    

    // WIN CHECK VALIDATION

    hasNoRemainingDarkRooms() {
    
        let rooms = document.getElementsByClassName('room');
        for (var i = 0; i < rooms.length; i++) {
    
            let roomClass = rooms[i].attributes[0].value;
            if ( !roomClass.includes("got-bulb") && !roomClass.includes("lights-on")) {

                return false;
            }
        }
    
        return true;
    },

    checkAllWallsContraints(n) {

        let table = this.getTable();  

        const wallsWithNumberZero = document.getElementsByClassName('wall-0');
        const wallsWithNumberOne = document.getElementsByClassName('wall-1');
        const wallsWithNumberTwo = document.getElementsByClassName('wall-2');
        const wallsWithNumberThree = document.getElementsByClassName('wall-3')
        
        if ( !this.checkWallConstraints(table, wallsWithNumberZero, n, 0) || 
             !this.checkWallConstraints(table, wallsWithNumberOne, n, 1) || 
             !this.checkWallConstraints(table, wallsWithNumberTwo, n, 2) || 
             !this.checkWallConstraints(table, wallsWithNumberThree, n, 3) ) {
            
            return false;
        }
        

        return true;
    },
    

    checkWallConstraints(table, walls, n, quantity) {

        for (var i = 0; i < walls.length; i++) {

            if (!this.checkWallConstraint(table, walls[i], n, quantity) ) {
                return false;
            }
        }

        return true;
    }, 

    checkWallConstraint(table, wall, n, quantity) {

        let row = parseInt(wall.dataset.row);
        let col = parseInt(wall.dataset.col);
        
        let bulbsCountInNeighbourhood = 0;
        if (row > 0 && col > 0 && row < n - 1 && col < n - 1) {

            if ( table[row - 1][col].attributes[0].value.includes("got-bulb") ) { bulbsCountInNeighbourhood++; }
            if ( table[row + 1][col].attributes[0].value.includes("got-bulb") ) { bulbsCountInNeighbourhood++; }
            if ( table[row][col - 1].attributes[0].value.includes("got-bulb") ) { bulbsCountInNeighbourhood++; }
            if ( table[row][col + 1].attributes[0].value.includes("got-bulb") ) { bulbsCountInNeighbourhood++; }

            if (bulbsCountInNeighbourhood !== quantity) {
                return false;
            }
        }

        if (row == 0 && col > 0 && col < n - 1) {
            
            bulbsCountInNeighbourhood = 0;

            if (table[row][col - 1].attributes[0].value.includes("got-bulb")) { bulbsCountInNeighbourhood++; }
            if (table[row][col + 1].attributes[0].value.includes("got-bulb")) { bulbsCountInNeighbourhood++; }
            if (table[row + 1][col].attributes[0].value.includes("got-bulb")) { bulbsCountInNeighbourhood++; }

            if (bulbsCountInNeighbourhood !== quantity) {
                return false;
            }
        }

        if (row == n - 1 && col > 0 && col < n - 1) {

            bulbsCountInNeighbourhood = 0;

            if (table[row][col - 1].attributes[0].value.includes("got-bulb")) { bulbsCountInNeighbourhood++; }
            if (table[row][col + 1].attributes[0].value.includes("got-bulb")) { bulbsCountInNeighbourhood++; }
            if (table[row - 1][col].attributes[0].value.includes("got-bulb")) { bulbsCountInNeighbourhood++; }

            if (bulbsCountInNeighbourhood !== quantity) {
                return false;
            }
        }

        if (col == 0 && row > 0 && row < n - 1) {

            bulbsCountInNeighbourhood = 0;

            if (table[row - 1][col].attributes[0].value.includes("got-bulb")) { bulbsCountInNeighbourhood++; }
            if (table[row + 1][col].attributes[0].value.includes("got-bulb")) { bulbsCountInNeighbourhood++; }
            if (table[row ][col + 1].attributes[0].value.includes("got-bulb")) { bulbsCountInNeighbourhood++; }

            if (bulbsCountInNeighbourhood !== quantity) {
                return false;
            }
        }

        if (col == n - 1 && row > 0 && row < n - 1) {

            bulbsCountInNeighbourhood = 0;

            if (table[row - 1][col].attributes[0].value.includes("got-bulb")) { bulbsCountInNeighbourhood++; }
            if (table[row + 1][col].attributes[0].value.includes("got-bulb")) { bulbsCountInNeighbourhood++; }
            if (table[row ][col - 1].attributes[0].value.includes("got-bulb")) { bulbsCountInNeighbourhood++; }

            if (bulbsCountInNeighbourhood !== quantity) {
                return false;
            }
        }

        if (row == 0 && col == 0) {

            bulbsCountInNeighbourhood = 0;

            if (table[row][col + 1].attributes[0].value.includes("got-bulb")) { bulbsCountInNeighbourhood++; }
            if (table[row + 1][col].attributes[0].value.includes("got-bulb")) { bulbsCountInNeighbourhood++; }

            if (bulbsCountInNeighbourhood !== quantity) {
                return false;
            }
        }

        if (row == n - 1 && col == 0) {

            bulbsCountInNeighbourhood = 0;

            if (table[row][col + 1].attributes[0].value.includes("got-bulb")) { bulbsCountInNeighbourhood++; }
            if (table[row - 1][col].attributes[0].value.includes("got-bulb")) { bulbsCountInNeighbourhood++; }

            if (bulbsCountInNeighbourhood !== quantity) {
                return false;
            }
        }

        if (row == 0 && col == n - 1) {

            bulbsCountInNeighbourhood = 0;

            if (table[row][col - 1].attributes[0].value.includes("got-bulb")) { bulbsCountInNeighbourhood++; }
            if (table[row + 1][col].attributes[0].value.includes("got-bulb")) { bulbsCountInNeighbourhood++; }

            if (bulbsCountInNeighbourhood !== quantity) {
                return false;
            }
        }

        if (row == n - 1 && col == n - 1) {

            bulbsCountInNeighbourhood = 0;

            if (table[row - 1][col].attributes[0].value.includes("got-bulb")) { bulbsCountInNeighbourhood++; }
            if (table[row][col - 1].attributes[0].value.includes("got-bulb")) { bulbsCountInNeighbourhood++; }

            if (bulbsCountInNeighbourhood !== quantity) {
                return false;
            }
        }

        return true;
    },

    // CLEAN UP WHEN LOSE

    removeCellClickListeners() {
    
        const cells = document.getElementsByClassName("room");
        for (var i = 0; i < cells.length; i++) {
            cells[i].removeEventListener('click', onRoomCellClick);
        }
    },
    
     // LIGHTS ON METHODS
    
    lightUpNeighborRooms(selectedRow, selectedCol) {
        
        this.lightUpRoomsVerticalBefore(selectedRow, selectedCol);
        this.lightUpRoomsVerticalAfter(selectedRow, selectedCol);
        this.lightUpRoomsHorizontalBefore(selectedRow, selectedCol);
        this.lightUpRoomsHorizontalAfter(selectedRow, selectedCol);
    },
    
    lightUpRoomsVerticalBefore(selectedRow, selectedCol) {
    
        let table = this.getTable();
        for (var i = selectedRow; i >= 0; i--) {
            
            let cellClass = table[i][selectedCol].attributes[0].value;
            if (cellClass.includes("wall")) { 
                break;
            }
    
            if ( cellClass.includes("room") && !cellClass.includes("got-bulb") ) {
                table[i][selectedCol].setAttribute('class', 'cell room lights-on');
            }
        }
    
    },
    
    lightUpRoomsVerticalAfter(selectedRow, selectedCol) {
    
        let table = this.getTable();
        for (var i = selectedRow; i < table[selectedRow].length; i++) {
            
            let cellClass = table[i][selectedCol].attributes[0].value;
            
            if (cellClass.includes("wall")) { 
                break;
            }
    
            if ( cellClass.includes("room") && !cellClass.includes("got-bulb") ) {
                table[i][selectedCol].setAttribute('class', 'cell room lights-on');
            }
        }
    },
    
    lightUpRoomsHorizontalBefore(selectedRow, selectedCol) {
    
        let table = this.getTable();
        for (var j = selectedCol; j >= 0; j--) {
            
            let cellClass = table[selectedRow][j].attributes[0].value;
            
            if (cellClass.includes("wall")) { 
                break;
            }
    
            if ( cellClass.includes("room") && !cellClass.includes("got-bulb") ) {
                table[selectedRow][j].setAttribute('class', 'cell room lights-on');
            }
        }
    },
    
    lightUpRoomsHorizontalAfter(selectedRow, selectedCol) {
    
        let table = this.getTable();
        for (var j = selectedCol; j < table[selectedRow].length; j++) {
            
            let cellClass = table[selectedRow][j].attributes[0].value;
            
            if (cellClass.includes("wall")) { 
                break;
            }
    
            if ( cellClass.includes("room") && !cellClass.includes("got-bulb") ) {
                table[selectedRow][j].setAttribute('class', 'cell room lights-on');
            }
        }
    },

    resetResultBoard() {

        const resultBoard = document.querySelector("#result-board");
        resultBoard.setAttribute('class', 'result-board');
        resultBoard.textContent = '';
    },

    updateResultBoard(resultType) {
        
        const resultBoard = document.querySelector("#result-board");

        if (resultType === RESULT_TYPE_LOSE) {
            
            resultBoard.setAttribute('class', 'result-board result-board-lose');
            resultBoard.textContent = RESULT_BOARD_TEXT_LOSE;
        }

        if (resultType === RESULT_TYPE_WIN) {
            resultBoard.setAttribute('class', 'result-board result-board-win');
            resultBoard.textContent = RESULT_BOARD_TEXT_WIN;
        }
    },

    startTimer() {

        const startTime = new Date();
        let hours = startTime.getHours();
        let minutes = startTime.getMinutes();
        let seconds = startTime.getSeconds();
        
        hours = this.checkTime(hours);
        minutes = this.checkTime(minutes);
        seconds = this.checkTime(seconds);
        document.querySelector('#timer').innerHTML =  hours + ":" + minutes + ":" + seconds;
        setTimeout(this.startTimer(), 1000);
    },

    checkTime(i) {

        if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
        return i;
    },

    resetTimer() {
        //TODO: impl. missing
    }

}